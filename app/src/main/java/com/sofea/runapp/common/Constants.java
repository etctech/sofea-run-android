package com.sofea.runapp.common;

import android.support.annotation.IntDef;

public class Constants {

    public static final int LOCATION_INTERVAL = 10000;
    public static final int FASTEST_LOCATION_INTERVAL = 5000;

    @IntDef({PlanType.GO, PlanType.POUCH, PlanType.GO_RUN, PlanType.GO_BUY, PlanType.GO_FUND})
    public @interface PlanType {
        int GO = 1;
        int POUCH = 2;
        int GO_RUN = 3;
        int GO_BUY = 4;
        int GO_FUND = 5;
    }

    @IntDef({APIStatus.SUCCESS, APIStatus.FAILED})
    public @interface APIStatus {
        int SUCCESS = 1;
        int FAILED = 0;
    }

    public interface ACTION {
        public static String MAIN_ACTION = "com.marothiatechs.foregroundservice.action.main";
        public static String INIT_ACTION = "com.marothiatechs.foregroundservice.action.init";
        public static String PREV_ACTION = "com.marothiatechs.foregroundservice.action.prev";
        public static String PLAY_ACTION = "com.marothiatechs.foregroundservice.action.play";
        public static String NEXT_ACTION = "com.marothiatechs.foregroundservice.action.next";
        public static String STARTFOREGROUND_ACTION = "com.marothiatechs.foregroundservice.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.marothiatechs.foregroundservice.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}
