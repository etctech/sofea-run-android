package com.sofea.runapp.ui.more;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityPrivacyPolicyBinding;
import com.sofea.runapp.ui.BaseActivity;

public class PrivacyPolicyActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, PrivacyPolicyActivity.class);
        context.startActivity(starter);
    }

    private ActivityPrivacyPolicyBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_privacy_policy);
        setTintBackButtonIcon(R.color.white, R.drawable.ic_back);
        setDisplayHomeAsUpEnabled();
        setBackNavigation();
    }
}
