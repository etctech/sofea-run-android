package com.sofea.runapp.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivitySplashScreenBinding;
import com.sofea.runapp.ui.login.LoginActivity;

public class SplashScreenActivity extends BaseActivity {

    private ActivitySplashScreenBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        binding.appName.postDelayed(new Runnable() {
            @Override
            public void run() {
                LoginActivity.start(SplashScreenActivity.this);
                finish();
            }
        }, 2000);
    }
}
