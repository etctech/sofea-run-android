package com.sofea.runapp.ui.plan;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.sofea.runapp.R;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.databinding.ActivityListViewBinding;
import com.sofea.runapp.model.Plan;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.databinding.FragmentListViewBinding;
import com.sofea.runapp.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PlanFragment extends BaseFragment {


    public static PlanFragment newInstance() {

        Bundle args = new Bundle();

        PlanFragment fragment = new PlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ActivityListViewBinding binding;
    private PlanAdapter adapter;
    private BaseActivity activity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) Objects.requireNonNull(getActivity());
        adapter = new PlanAdapter(activity, new ArrayList<Plan>());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_list_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        initPlans();

        Util.setViewShown(binding.progressContainer, false);
    }

    private void initPlans() {
        List<Plan> plans = new ArrayList<>();
        plans.add(new Plan(Constants.PlanType.GO, R.drawable.plan_go, R.color.plan_go, R.string.go)
                .setEnabled(true));
        plans.add(new Plan(Constants.PlanType.POUCH, R.drawable.plan_go_pouch, R.color.plan_pouch, R.string.pouch)
                .setEnabled(false));
        plans.add(new Plan(Constants.PlanType.GO_RUN, R.drawable.plan_go_run, R.color.plan_go_run, R.string.go_run)
                .setEnabled(false));
        plans.add(new Plan(Constants.PlanType.GO_BUY, R.drawable.plan_go_buy, R.color.plan_go_buy, R.string.go_buy)
                .setEnabled(false));
        plans.add(new Plan(Constants.PlanType.GO_FUND, R.drawable.plan_go_fund, R.color.plan_go_fund, R.string.go_run)
                .setEnabled(false));

        adapter.addAll(plans);
    }

    private void initView() {
        binding.toolbarTitle.setText(R.string.plan);
        binding.swipeRefresh.setEnabled(false);

        binding.listView.setAdapter(adapter);
        binding.listView.setOnItemClickListener(onItemClickListener);
        binding.listView.hasMorePages(false);
        binding.listView.setEmptyView(binding.empty);
        binding.empty.setText(R.string.no_plans);

        int pad8 = Util.dpToPx(activity, 8);
        binding.listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
        binding.listView.setDividerHeight(pad8);
        binding.listView.setPadding(pad8, pad8, pad8, pad8);
        binding.listView.setClipToPadding(false);
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        }
    };
}

