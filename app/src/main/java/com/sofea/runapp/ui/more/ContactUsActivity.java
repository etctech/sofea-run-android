package com.sofea.runapp.ui.more;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityContactUsBinding;
import com.sofea.runapp.ui.BaseActivity;

public class ContactUsActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, ContactUsActivity.class);
        context.startActivity(starter);
    }

    private ActivityContactUsBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        setTintBackButtonIcon(R.color.white, R.drawable.ic_back);
        setDisplayHomeAsUpEnabled();
        setBackNavigation();
    }
}
