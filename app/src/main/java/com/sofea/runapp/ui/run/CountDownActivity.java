package com.sofea.runapp.ui.run;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityCountDownBinding;
import com.sofea.runapp.ui.BaseActivity;

public class CountDownActivity extends BaseActivity implements View.OnClickListener {

    public static void start(Activity activity, Fragment fragment, int requestCode) {
        Intent starter = new Intent(activity, CountDownActivity.class);
        fragment.startActivityForResult(starter, requestCode);
    }

    private ActivityCountDownBinding binding;
    private CountDownTimer countDownTimer;
    private long countdownPeriod;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_count_down);
        countdownPeriod = 15000;
        createCountDownTimer();

        binding.timer.setOnClickListener(this);
        binding.add.setOnClickListener(this);
    }

    private void createCountDownTimer() {
        countDownTimer = new CountDownTimer(countdownPeriod + 1000, 1) {

            @Override
            public void onTick(long millisUntilFinished) {
                binding.timer.setText(String.valueOf(millisUntilFinished / 1000));
                countdownPeriod = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                finish();
            }
        };
        countDownTimer.start();
    }

    @Override
    public void finish() {
        setResult(Activity.RESULT_OK);
        super.finish();
    }

    @Override
    public void onClick(View view) {
        if (view == binding.timer) {
            finish();
        } else if (view == binding.add) {
            countdownPeriod += 10000;
            if (countDownTimer != null)
                countDownTimer.cancel();
            createCountDownTimer();
        }
    }
}
