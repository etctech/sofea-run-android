package com.sofea.runapp.ui.event;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.sofea.runapp.MyApplication;
import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityEventDetailsBinding;
import com.sofea.runapp.event.EventsEvent;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.job.JoinEventJob;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.ImageLoader;
import com.sofea.runapp.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.joda.time.Days;

public class EventDetailsActivity extends BaseActivity {

    private final int hashCode = hashCode();

    public static void start(Context context, Event event) {
        Intent starter = new Intent(context, EventDetailsActivity.class);
        starter.putExtra(EVENT, event.toJson());
        context.startActivity(starter);
    }

    private static final String EVENT = "event";
    private ActivityEventDetailsBinding binding;
    private Event event;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_event_details);
        setTintBackButtonIcon(R.color.white, R.drawable.ic_back);
        setDisplayHomeAsUpEnabled();
        setBackNavigation();

        event = Event.deserialize(getIntent().getStringExtra(EVENT));
        setToolbarTitle(event.getTitle());

        binding.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinEvent();
            }
        });

        populateEvent();
    }

    private void joinEvent() {
        showLoadingDialog();
        MyApplication.addJobInBackground(new JoinEventJob(event.getId(), hashCode()));
    }

    private void populateEvent() {
        ImageLoader.glideImageLoadCenterCrop(binding.image, R.mipmap.ic_launcher, event.getCoverUrl());
        binding.title.setText(event.getTitle());
        binding.location.setText(event.getLocation());
        binding.status.setText(event.getStatus());
        binding.totalRunner.setText(String.valueOf(event.getTotalRunners()));

        DateTime startDate = new DateTime(event.getStartDate().replace(" ", "T"));
        binding.startDate.setText(startDate.toString("dd MMM"));

        int days = Days.daysBetween(DateTime.now(), startDate).getDays();
        days = days < 0 ? days * -1 : days;
        binding.dateLeft.setText(String.format(getString(R.string.d_days_until), days));

        binding.description.setText(event.getDescription());
        binding.reward.setText(event.getReward());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(EventsEvent.OnJoinEvent event) {
        ToastUtil.show(this, "Success!!");
        dismissLoadingDialog();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(ExceptionEvent event) {
        if (hashCode == event.getHashCode()) {
            ToastUtil.show(this, event.getErrorMessage());
            dismissLoadingDialog();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusUtil.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusUtil.unregister(this);
    }
}
