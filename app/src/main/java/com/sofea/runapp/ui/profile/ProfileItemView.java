package com.sofea.runapp.ui.profile;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewProfileItemBinding;
import com.sofea.runapp.model.User;

public class ProfileItemView extends FrameLayout {
    public ProfileItemView(@NonNull Context context) {
        super(context);
        init();
    }

    public ProfileItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public ProfileItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private ViewProfileItemBinding binding;

    private void init() {
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.view_profile_item,
                    this,
                    true);
        }
    }

    public ProfileItemView setTitle(String title) {
        binding.title.setText(title);
        return this;
    }

    public ProfileItemView setValue(String value) {
        binding.value.setText(value);
        return this;
    }

    //    String.format("%.2f", d)
    public ProfileItemView setValue(String format, double value) {
        binding.value.setText(String.format(format, value));
        return this;
    }

    public ProfileItemView setValue(int value) {
        binding.value.setText(String.valueOf(value));
        return this;
    }
}
