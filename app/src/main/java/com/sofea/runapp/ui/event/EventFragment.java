package com.sofea.runapp.ui.event;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.sofea.runapp.MyApplication;
import com.sofea.runapp.R;
import com.sofea.runapp.databinding.FragmentListViewBinding;
import com.sofea.runapp.event.EventsEvent;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.job.GetEventJob;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.ToastUtil;
import com.sofea.runapp.util.Util;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

public class EventFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private final int hashCode = hashCode();

    public static EventFragment newInstance() {

        Bundle args = new Bundle();

        EventFragment fragment = new EventFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FragmentListViewBinding binding;
    private EventAdapter adapter;
    private BaseActivity activity;
    private EventHeaderView headerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) Objects.requireNonNull(getActivity());
        adapter = new EventAdapter(activity, new ArrayList<Event>());
        headerView = new EventHeaderView(activity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_view, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.swipeRefresh.setEnabled(false);

        binding.listView.setAdapter(adapter);
        binding.listView.hasMorePages(false);
        binding.listView.setOnItemClickListener(this);
        binding.listView.addHeaderView(headerView);
        headerView.update("http://movingwallpapers.net/Desktop-Wallpapers/run-Wallpaper-For-Mac-number-0dX.jpg",
                getString(R.string.ongoing_and_upcoming_events));

        /*List<Event> events = new ArrayList<>();
        events.add(new Event().setImageUrl("https://us.123rf.com/450wm/makstrv/makstrv1511/makstrv151100043/47563362-stock-vector-pirate-skull-and-swords-symbol-mascot-logo-.jpg?ver=6")
                .setTitle("PIRATE RUN")
                .setCoverUrl("https://images.unsplash.com/photo-1452626038306-9aae5e071dd3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1002f970f615e046c7c5376c46e13bef&w=1000&q=80")
                .setLocation("Jalan Dutamas 1, Solaris Dutams, 50480 Kuala Lumpur, Federal Territory of Kuala Lumpur")
                .setTotalRunners(3400)
                .setDescription("Lorem ipsum dolor sit amet, nunc amet est eu suspendisse, etiam nullam ac nulla, dignissim ut et fusce tellus. Venenatis nunc ultrices tortor. Ligula mi bibendum purus elit in, velit purus tincidunt quam, tristique a a tincidunt non metus vestibulum, erat laudantium risus, feugiat eu. Eu etiam vel quis massa, leo ante nunc pellentesque erat nec. Senectus tincidunt. Ut sollicitudin, cras ultrices sollicitudin mi molestie, cursus dis. Vel massa nunc est ut, tortor aenean enim. Pellentesque metus dui proin, vestibulum sed est tincidunt et maecenas. Dolor sed pellentesque est nulla ac laoreet. Neque enim maecenas quis viverra ac dictum, sed habitasse laoreet sollicitudin, vel distinctio libero malesuada at sollicitudin, ut et in metus dolor parturient. Nullam in duis, vitae ipsum mollis enim, sed quasi erat. Metus vestibulum vivamus donec consequat lorem tempus, ut leo venenatis sed ac ut, est est sed rhoncus, ea elit condimentum sed.\n" +
                        "\n" +
                        "Mi curabitur, parturient a sit. Laoreet taciti viverra rhoncus rutrum, ullamcorper dictum dignissim platea natoque facilisis nec, a donec fermentum scelerisque duis, egestas vulputate mus hac, vehicula amet pulvinar per ornare justo. Diam nec mauris tristique lobortis quam. Ipsum et vehicula ligula, maecenas sed porttitor faucibus sit, neque nulla rhoncus justo. Fermentum aptent a eros in, nulla consectetuer lorem augue nec dapibus leo.")
                .setReward("Orci aliquam mauris rutrum. Ipsum velit a gravida, velit lacinia non erat turpis est semper. Wisi quis rhoncus, justo quam, eget ad a tempus amet nunc, faucibus enim est a qui tellus bibendum, wisi diam. Montes velit fusce dignissim suspendisse. Amet velit, vestibulum in. Dolor dui integer, sapien condimentum tortor mattis quisque lectus cras. Pellentesque rhoncus montes, feugiat tristique porta et. Proin luctus, id est nulla praesent tortor vestibulum dui, a autem et massa. Arcu vel praesent, odio pede laoreet vestibulum.")
                .setStartDate("2018-08-01 08:00:00")
                .setStatus("Open"));
        events.add(new Event().setImageUrl("https://image.freepik.com/free-vector/ninja-logo-abstract-modern-shape_20065-37.jpg")
                .setTitle("NINJA RUN")
                .setCoverUrl("https://cdn.lifehack.org/wp-content/uploads/2014/03/runners-960px-wd.jpg")
                .setLocation("Jalan Dutamas 1, Solaris Dutams, 50480 Kuala Lumpur, Federal Territory of Kuala Lumpur")
                .setTotalRunners(3400)
                .setDescription("Lorem ipsum dolor sit amet, nunc amet est eu suspendisse, etiam nullam ac nulla, dignissim ut et fusce tellus. Venenatis nunc ultrices tortor. Ligula mi bibendum purus elit in, velit purus tincidunt quam, tristique a a tincidunt non metus vestibulum, erat laudantium risus, feugiat eu. Eu etiam vel quis massa, leo ante nunc pellentesque erat nec. Senectus tincidunt. Ut sollicitudin, cras ultrices sollicitudin mi molestie, cursus dis. Vel massa nunc est ut, tortor aenean enim. Pellentesque metus dui proin, vestibulum sed est tincidunt et maecenas. Dolor sed pellentesque est nulla ac laoreet. Neque enim maecenas quis viverra ac dictum, sed habitasse laoreet sollicitudin, vel distinctio libero malesuada at sollicitudin, ut et in metus dolor parturient. Nullam in duis, vitae ipsum mollis enim, sed quasi erat. Metus vestibulum vivamus donec consequat lorem tempus, ut leo venenatis sed ac ut, est est sed rhoncus, ea elit condimentum sed.\n" +
                        "\n" +
                        "Mi curabitur, parturient a sit. Laoreet taciti viverra rhoncus rutrum, ullamcorper dictum dignissim platea natoque facilisis nec, a donec fermentum scelerisque duis, egestas vulputate mus hac, vehicula amet pulvinar per ornare justo. Diam nec mauris tristique lobortis quam. Ipsum et vehicula ligula, maecenas sed porttitor faucibus sit, neque nulla rhoncus justo. Fermentum aptent a eros in, nulla consectetuer lorem augue nec dapibus leo.")
                .setReward("Orci aliquam mauris rutrum. Ipsum velit a gravida, velit lacinia non erat turpis est semper. Wisi quis rhoncus, justo quam, eget ad a tempus amet nunc, faucibus enim est a qui tellus bibendum, wisi diam. Montes velit fusce dignissim suspendisse. Amet velit, vestibulum in. Dolor dui integer, sapien condimentum tortor mattis quisque lectus cras. Pellentesque rhoncus montes, feugiat tristique porta et. Proin luctus, id est nulla praesent tortor vestibulum dui, a autem et massa. Arcu vel praesent, odio pede laoreet vestibulum.")
                .setStartDate("2018-08-31 08:00:00")
                .setStatus("Open"));
        adapter.addAll(events);*/
        refreshEvents();
    }

    public void refreshEvents() {
        adapter.clear();
        Util.setViewShown(binding.progressContainer, true);
        MyApplication.addJobInBackground(new GetEventJob(hashCode));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Object object = binding.listView.getItemAtPosition(i);
        if (object != null && object instanceof Event) {
            EventDetailsActivity.start(activity, (Event) object);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(EventsEvent.OnGetAllEvents event) {
        ArrayList<Event> lists = event.getList();
        if (lists != null && lists.size() > 0)
            adapter.addAll(lists);
        Util.setViewShown(binding.progressContainer, false);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(ExceptionEvent event) {
        if (hashCode == event.getHashCode()) {
            ToastUtil.show(activity, event.getErrorMessage());
            Util.setViewShown(binding.progressContainer, false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusUtil.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusUtil.unregister(this);
    }
}
