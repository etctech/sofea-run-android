package com.sofea.runapp.ui.event;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewEventItemBinding;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.model.Notification;
import com.sofea.runapp.util.ImageLoader;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventItemView extends FrameLayout {

    private ViewEventItemBinding binding;

    public EventItemView(@NonNull Context context) {
        super(context);
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                    R.layout.view_event_item,
                    this,
                    true);
        }
    }

    public void update(Event event) {
        ImageLoader.glideImageLoadCenterCrop(binding.image, R.drawable.splash_screen_icon, event.getImageUrl());
        binding.title.setText(event.getTitle());
        binding.date.setText(((new DateTime(event.getStartDate()).toString("EEE, dd MMM    "))+event.getStartTime()));
        binding.status.setText(event.getStatus());
    }
}
