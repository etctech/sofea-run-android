package com.sofea.runapp.ui;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.sofea.runapp.R;
import com.sofea.runapp.widget.LoadingDialogFragment;

public abstract class BaseActivity extends AppCompatActivity {

    private LoadingDialogFragment loadingDialogFragment;
    private Toolbar actionbarToolbar;
    private TextView toolbarTitle;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        getActionbarToolbar();
        getToolbarTitle();
    }

    @Nullable
    protected TextView getToolbarTitle() {
        if (toolbarTitle == null) {
            toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        }
        return toolbarTitle;
    }

    @Nullable
    protected Toolbar getActionbarToolbar() {
        if (actionbarToolbar == null) {
            actionbarToolbar = (Toolbar) findViewById(R.id.actionbar_toolbar);
            if (actionbarToolbar != null) {
                setSupportActionBar(actionbarToolbar);
            }
        }
        return actionbarToolbar;
    }

    protected void setDisplayHomeAsUpEnabled() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void setTintBackButtonIcon(@ColorRes int colorId, @DrawableRes int drawableId) {
        if (getSupportActionBar() != null) {
            Drawable drawable = ContextCompat.getDrawable(this, drawableId);
            drawable.setColorFilter(ContextCompat.getColor(this, colorId), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(drawable);
        }
    }

    protected void setBackNavigation() {
        if (actionbarToolbar != null) {
            actionbarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void setToolbarTitle(@StringRes int titleId) {
        if (toolbarTitle != null) {
            toolbarTitle.setText(titleId);
        }
    }

    public void setToolbarTitle(String title) {
        if (toolbarTitle != null) {
            toolbarTitle.setText(title);
        }
    }

    public void showLoadingDialog() {
        if (loadingDialogFragment != null) loadingDialogFragment.dismiss();
        loadingDialogFragment = new LoadingDialogFragment();
        loadingDialogFragment.show(getSupportFragmentManager(), "");
    }

    public void dismissLoadingDialog() {
        if (loadingDialogFragment != null && loadingDialogFragment.isAdded()) {
            loadingDialogFragment.dismiss();
        }
    }
}
