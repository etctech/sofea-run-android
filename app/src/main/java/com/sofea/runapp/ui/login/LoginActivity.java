package com.sofea.runapp.ui.login;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.sofea.runapp.MyApplication;
import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityLoginBinding;
import com.sofea.runapp.event.AccountEvent;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.job.LoginJob;
import com.sofea.runapp.model.User;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.util.ToastUtil;
import com.sofea.runapp.ui.MainActivity;
import com.sofea.runapp.util.EventBusUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private CallbackManager callbackManager;
    private final static int RC_SIGN_IN = 9901;

    private GoogleSignInClient mGoogleSignInClient;

    public static void start(Context context) {
        Intent starter = new Intent(context, LoginActivity.class);
        context.startActivity(starter);
    }

    private ActivityLoginBinding binding;
    private final int hashCode = hashCode();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        initFacebookLogin();
        initGoogleLogin();

        binding.facebookLogin.setOnClickListener(this);
        binding.gmailLogin.setOnClickListener(this);
    }

    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
    }

    private void initGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void onClick(View view) {
        if (view == binding.facebookLogin) {
//            attemptFacebookLogin();
//            binding.fbLoginButton.callOnClick();
        } else if (view == binding.gmailLogin) {
            attemptGoogleLogin();
        }

        showLoadingDialog();
        MyApplication.addJobInBackground(new LoginJob(new User(), hashCode));
        finish();
    }

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            ToastUtil.show(this, getString(R.string.tap_again_exit));
        }

        mBackPressed = System.currentTimeMillis();
    }


    private void attemptFacebookLogin() {

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
    }


    private void attemptGoogleLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        revokeAccess();
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            ToastUtil.show(this, account.getEmail());
            //Get Login Details
            //Call API
            //Google Logout
        } catch (ApiException e) {
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(AccountEvent.OnLogin event) {
        if (hashCode == event.getHashCode()) {
            MainActivity.start(this);
            dismissLoadingDialog();
            finish();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(ExceptionEvent event) {
        if (hashCode == event.getHashCode()) {
            ToastUtil.show(this, event.getErrorMessage());
            dismissLoadingDialog();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBusUtil.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBusUtil.unregister(this);
    }
}
