package com.sofea.runapp.ui.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofea.runapp.MyApplication;
import com.sofea.runapp.R;
import com.sofea.runapp.event.AccountEvent;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.job.GetProfileJob;
import com.sofea.runapp.model.User;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.databinding.FragmentProfileBinding;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.ImageLoader;
import com.sofea.runapp.util.ToastUtil;
import com.sofea.runapp.util.UserHandler;
import com.sofea.runapp.util.Util;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

public class ProfileFragment extends BaseFragment {

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FragmentProfileBinding binding;
    private BaseActivity activity;
    private final int hashCode = hashCode();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) Objects.requireNonNull(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();

        Util.setListShown(binding.container, binding.progressContainer, false, false);
        MyApplication.addJobInBackground(new GetProfileJob(hashCode));
    }

    private void populateUser() {
        User user = UserHandler.getUser(activity);
        ImageLoader.glideImageLoadCenterCrop(binding.profile, R.drawable.splash_screen_icon, user.getProfileUrl());
        binding.name.setText(user.getName());
        binding.location.setText(user.getLocation());

        binding.totalKm.setValue("%.1f", user.getTotalKM());
        binding.minSec.setValue("%.2f", user.getMinPerSec());
        binding.totalRuns.setValue(user.getTotalRuns());
        binding.calories.setValue(user.getCalories());
    }

    private void initView() {
        binding.totalKm.setTitle(getString(R.string.total_km));
        binding.minSec.setTitle(getString(R.string.min_sec));
        binding.totalRuns.setTitle(getString(R.string.total_runs));
        binding.calories.setTitle(getString(R.string.caloris));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(AccountEvent.OnGetProfile event) {
        populateUser();
        Util.setListShown(binding.container, binding.progressContainer, true, true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(ExceptionEvent event) {
        if (hashCode == event.getHashCode()) {
            ToastUtil.show(activity, event.getErrorMessage());
            Util.setListShown(binding.container, binding.progressContainer, true, true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusUtil.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusUtil.unregister(this);
    }
}
