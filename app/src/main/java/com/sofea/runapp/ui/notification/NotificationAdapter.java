package com.sofea.runapp.ui.notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sofea.runapp.model.Notification;

import java.util.List;

public class NotificationAdapter extends ArrayAdapter<Notification> {
    public NotificationAdapter(@NonNull Context context, @NonNull List<Notification> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = new NotificationItemView(getContext());
        }

        ((NotificationItemView) convertView).update(getItem(position));
        return convertView;
    }
}
