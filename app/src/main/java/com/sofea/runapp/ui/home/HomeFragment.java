package com.sofea.runapp.ui.home;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.FragmentHomeBinding;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.ui.event.EventFragment;
import com.sofea.runapp.ui.more.MoreFragment;
import com.sofea.runapp.ui.notification.NotificationActivity;
import com.sofea.runapp.ui.notification.NotificationAdapter;
import com.sofea.runapp.ui.plan.PlanFragment;
import com.sofea.runapp.ui.profile.ProfileFragment;
import com.sofea.runapp.ui.run.RunFragment;
import com.sofea.runapp.ui.setting.SettingFragment;
import com.sofea.runapp.widget.ViewPagerAdapter;

import java.nio.DoubleBuffer;
import java.util.Objects;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FragmentHomeBinding binding;
    private ViewPagerAdapter viewPagerAdapter;
    private BaseActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        activity = (BaseActivity) Objects.requireNonNull(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewPagerAdapter.addFragment(ProfileFragment.newInstance(), getString(R.string.my_profile));
        viewPagerAdapter.addFragment(EventFragment.newInstance(), getString(R.string.events));
        binding.viewPager.setAdapter(viewPagerAdapter);
        binding.viewPager.addOnPageChangeListener(onPageChangeListener);
        binding.indicator.setViewPager(binding.viewPager);
        onPageChangeListener.onPageSelected(0);

        binding.notification.setOnClickListener(this);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            binding.toolbarTitle.setText(viewPagerAdapter.getPageTitle(position));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void onClick(View view) {
        if (view == binding.notification) {
            NotificationActivity.start(activity);
        }
    }
}
