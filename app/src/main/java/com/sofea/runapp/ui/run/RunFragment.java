package com.sofea.runapp.ui.run;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.sofea.runapp.R;
import com.sofea.runapp.databinding.FragmentRunBinding;
import com.sofea.runapp.event.PermissionEvent;
import com.sofea.runapp.model.Route;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.LocationUtil;
import com.sofea.runapp.util.PermissionUtil;
import com.sofea.runapp.util.ToastUtil;
import com.sofea.runapp.util.Util;
import com.sofea.runapp.util.run.RunHandler;
import com.sofea.runapp.util.run.RunServiceUtil;
import com.sofea.runapp.util.run.TimeUtil;
import com.sofea.runapp.util.service.LocationMonitoringService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class RunFragment extends BaseFragment implements View.OnClickListener {

    public static RunFragment newInstance() {

        Bundle args = new Bundle();

        RunFragment fragment = new RunFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private final int SELF_LOCATION_REQUEST_CODE = 101;
    private final int COUNT_DOWN_REQUEST_CODE = 102;
    private FragmentRunBinding binding;
    private GoogleMap googleMap;
    private SupportMapFragment mSupportMapFragment;
    private BaseActivity activity;
    private PolylineOptions polylineOptions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (BaseActivity) Objects.requireNonNull(getActivity());
        polylineOptions = new PolylineOptions()
                .color(ContextCompat.getColor(activity, R.color.colorPrimary))
                .endCap(new RoundCap())
                .jointType(JointType.ROUND);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_run, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.toolbarTitle.setText(R.string.run);
        binding.tapToRun.setOnClickListener(this);

        initializeMap();

        LocalBroadcastManager.getInstance(activity)
                .registerReceiver(broadcastReceiver,
                        new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
                );
        handleButton();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getParcelableExtra(LocationMonitoringService.LOCATION) != null) {
                Location location = (Location) intent.getParcelableExtra(LocationMonitoringService.LOCATION);
                boolean hasStarted = intent.getBooleanExtra(LocationMonitoringService.START_RUNNING, false);
                double distance = intent.getDoubleExtra(LocationMonitoringService.TOTAL_DISTANCE, 0);

                calculateKM(distance);
                LocationUtil.updateLocation(location);
                handleUpdatedLocation(location, hasStarted);
            } else {
                int totalSec = intent.getIntExtra(LocationMonitoringService.DURATION_SEC, 0);
                displayTotalRunTime(totalSec);
                binding.avgPace.setText(RunHandler.getAvgPace(activity));
            }
        }
    };

    private void displayTotalRunTime(int totalSec) {
        binding.duration.setText(TimeUtil.getDisplayRunTime(totalSec));
    }

    private void initializeMap() {

        mSupportMapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mSupportMapFragment).commit();
        }
        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap1) {
                    if (googleMap1 != null) {
                        googleMap = googleMap1;
                        updateLocationUI();
                    }
                }
            });
        }
    }

    private void updateLocationUI() {
        if (this.googleMap == null) {
            return;
        }
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtil.requestPermission(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, SELF_LOCATION_REQUEST_CODE);
            return;
        }

        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionUtil.isPermissionHasGranted(grantResults)) {
            if (SELF_LOCATION_REQUEST_CODE == requestCode) {
                updateLocationUI();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == binding.tapToRun) {
            RunHandler.setIsRunning(activity, true);
            CountDownActivity.start(activity, RunFragment.this, COUNT_DOWN_REQUEST_CODE);
        } else if (view == binding.lock) {
            Util.setViewShown(binding.stop, true);
            binding.stop.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Util.setViewShown(binding.stop, false);
                }
            }, 3000);
        } else if (view == binding.stop) {
            resetUi();
            RunHandler.stop(activity);
            handleButton();
            startRunningForService(false);
            resetMap();
        }
    }

    private void resetMap() {
        if (googleMap != null) {
            googleMap.clear();
        }
    }

    private void resetUi() {
        binding.distance.setText("0.00");
        binding.duration.setText("00:00");
        binding.avgPace.setText("00:00");

        Util.setViewShown(binding.stop, false);
        Util.setViewShown(binding.tapToRun, true);
        binding.lock.setVisibility(View.GONE);

        polylineOptions = new PolylineOptions()
                .color(ContextCompat.getColor(activity, R.color.colorPrimary))
                .endCap(new RoundCap())
                .jointType(JointType.ROUND);
        startRoute = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == COUNT_DOWN_REQUEST_CODE) {
                handleButton();
                startRunningForService(true);

            }
        }
    }

    private void startRunningForService(boolean start) {
        Intent broadcast = new Intent(LocationMonitoringService.DATA_ARGUMENT);
        broadcast.putExtra(LocationMonitoringService.START_RUNNING, start);
        activity.sendBroadcast(broadcast);
    }

    private void handleButton() {
        if (RunHandler.isRunning(activity)) {
            binding.tapToRun.setVisibility(View.GONE);
            binding.stop.setVisibility(View.GONE);
            binding.lock.setVisibility(View.VISIBLE);
            binding.lock.setOnClickListener(this);
            binding.stop.setOnClickListener(this);
        } else {
            binding.tapToRun.setVisibility(View.VISIBLE);
            binding.stop.setVisibility(View.GONE);
            binding.lock.setVisibility(View.GONE);
            binding.lock.setOnClickListener(null);
            binding.stop.setOnClickListener(null);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (RunHandler.isRunning(activity)) {
            startRoute = RunHandler.getStartingRoute(activity);
            List<Route> routes = RunHandler.getRoutes(activity);
            for (int i = 0; i < routes.size(); i++) {
                Route route = routes.get(i);
                polylineOptions.add(new LatLng(route.getLatitude(), route.getLongitude()));
            }
        }
        attemptStartRunning();
    }

    private static Route startRoute;

    private void handleUpdatedLocation(Location location, boolean hasStarted) {
        Route route = new Route(location);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.getLatitude(), route.getLongitude()), 17.0f));
        if (googleMap != null && hasStarted) {
            googleMap.clear();
            if (startRoute == null) {
                startRoute = route;
            }
            drawMap(route);
        }
    }

    private void drawMap(Route route) {
        drawMarker(startRoute);
        polylineOptions.add(new LatLng(route.getLatitude(), route.getLongitude()));
        googleMap.addPolyline(polylineOptions);
    }

    private void calculateKM(double totalKM) {
        binding.distance.setText(String.format(Locale.ENGLISH, "%.2f", totalKM));
    }

    private void drawMarker(Route route) {
        LatLng currentPosition = new LatLng(route.getLatitude(), route.getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .position(currentPosition)
                .icon(Util.getBitmapDescriptor(activity, R.drawable.ic_orange_marker)));
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBusUtil.unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBusUtil.register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHandle(PermissionEvent.OnPermissionGranted event) {
        updateLocationUI();
    }

    private void attemptStartRunning() {
        attemptStartRunning(null);
    }

    private void attemptStartRunning(@Nullable DialogInterface dialog) {
        if (isGooglePlayServicesAvailable()
                && hasInternetConnection(dialog)
                && RunServiceUtil.isRunningPermissionGranted(activity, SELF_LOCATION_REQUEST_CODE)) {
            startRunningService();
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        if (RunServiceUtil.isGooglePlayServicesAvailable(activity)) {
            return true;
        }
        Toast.makeText(activity, R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        return false;
    }

    private boolean hasInternetConnection(@Nullable DialogInterface dialog) {
        if (RunServiceUtil.hasInternetConnection(activity)) {
            if (dialog != null) {
                dialog.dismiss();
            }
            return true;
        }
        showInternetConnectionErrorDialog();
        return false;
    }

    private void showInternetConnectionErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.no_internet_title);
        builder.setMessage(R.string.msg_alert_no_internet);
        String positiveText = getString(R.string.refresh);

        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        attemptStartRunning(dialog);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean isRunningServiceStarted = false;

    private void startRunningService() {
        if (!isRunningServiceStarted) {
            Intent intent = new Intent(activity, LocationMonitoringService.class);
            activity.startService(intent);
            isRunningServiceStarted = true;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!RunHandler.isRunning(activity)) {
            activity.stopService(new Intent(activity, LocationMonitoringService.class));
        }
    }
}
