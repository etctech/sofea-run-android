package com.sofea.runapp.ui.plan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.sofea.runapp.model.Plan;

import java.util.List;

public class PlanAdapter extends ArrayAdapter<Plan> {
    public PlanAdapter(@NonNull Context context, @NonNull List<Plan> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = new PlanItemView(getContext());
        }

        ((PlanItemView) convertView).update(getItem(position));
        return convertView;
    }
}
