package com.sofea.runapp.ui.more;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityFaqBinding;
import com.sofea.runapp.ui.BaseActivity;

public class FAQActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, FAQActivity.class);
        context.startActivity(starter);
    }

    private ActivityFaqBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_faq);
        setTintBackButtonIcon(R.color.white, R.drawable.ic_back);
        setDisplayHomeAsUpEnabled();
        setBackNavigation();
    }
}
