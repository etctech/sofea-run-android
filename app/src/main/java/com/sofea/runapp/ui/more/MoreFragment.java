package com.sofea.runapp.ui.more;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sofea.runapp.R;
import com.sofea.runapp.ui.BaseFragment;
import com.sofea.runapp.databinding.FragmentMoreBinding;

public class MoreFragment extends BaseFragment implements View.OnClickListener {

    public static MoreFragment newInstance() {

        Bundle args = new Bundle();

        MoreFragment fragment = new MoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private FragmentMoreBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.contactUs.setOnClickListener(this);
        binding.faq.setOnClickListener(this);
        binding.tac.setOnClickListener(this);
        binding.privacyPolicy.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == binding.contactUs) {
            ContactUsActivity.start(getActivity());
        } else if (view == binding.faq) {
            FAQActivity.start(getActivity());
        } else if (view == binding.tac) {
            TermAndConditionActivity.start(getActivity());
        } else if (view == binding.privacyPolicy) {
            PrivacyPolicyActivity.start(getActivity());
        }
    }
}

