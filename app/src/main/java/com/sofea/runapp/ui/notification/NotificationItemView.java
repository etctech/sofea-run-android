package com.sofea.runapp.ui.notification;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewNotificationItemBinding;
import com.sofea.runapp.model.Notification;
import com.sofea.runapp.util.ImageLoader;

import org.joda.time.DateTime;

public class NotificationItemView extends FrameLayout {

    private ViewNotificationItemBinding binding;

    public NotificationItemView(@NonNull Context context) {
        super(context);
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                    R.layout.view_notification_item,
                    this,
                    true);
        }
    }

    public void update(Notification notification) {
        ImageLoader.glideImageLoadCenterCrop(binding.image, R.drawable.splash_screen_icon, notification.getImageUrl());
        binding.title.setText(notification.getTitle());

        DateTime createDate = new DateTime(notification.getCreatedDate().replace(" ", "T"));
        String displayDate = DateUtils.getRelativeTimeSpanString(createDate.getMillis(), DateTime.now().getMillis(), DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        binding.date.setText(displayDate);
    }
}
