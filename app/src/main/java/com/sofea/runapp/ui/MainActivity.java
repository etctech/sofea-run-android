package com.sofea.runapp.ui;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.sofea.runapp.MyApplication;
import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityMainBinding;
import com.sofea.runapp.event.PermissionEvent;
import com.sofea.runapp.job.GetProfileJob;
import com.sofea.runapp.ui.home.HomeFragment;
import com.sofea.runapp.ui.more.MoreFragment;
import com.sofea.runapp.ui.plan.PlanFragment;
import com.sofea.runapp.ui.profile.ProfileFragment;
import com.sofea.runapp.ui.run.RunFragment;
import com.sofea.runapp.ui.setting.SettingFragment;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.PermissionUtil;
import com.sofea.runapp.util.ToastUtil;
import com.sofea.runapp.widget.BottomNavigationItemView;
import com.sofea.runapp.widget.ViewPagerAdapter;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    @ColorRes
    private static final int unselectedTabColor = R.color.white;
    @ColorRes
    private static final int selectedTabColor = R.color.colorPrimary;
    private ActivityMainBinding binding;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initBottomView();
        initNavViewPager();

        binding.tabSetting.setVisibility(View.GONE);
    }

    private void initBottomView() {
        binding.tabHome.setTitle(getString(R.string.home))
                .setIcon(R.drawable.ic_home)
                .setTint(unselectedTabColor)
                .setOnClickListener(this);

        binding.tabPlan.setTitle(getString(R.string.plan))
                .setIcon(R.drawable.ic_calendar)
                .setTint(unselectedTabColor)
                .setOnClickListener(this);

        binding.tabRun.setTitle(getString(R.string.run))
                .setIcon(R.drawable.ic_run)
                .setTint(unselectedTabColor)
                .setOnClickListener(this);

        binding.tabSetting.setTitle(getString(R.string.setting))
                .setIcon(R.drawable.ic_settings)
                .setTint(unselectedTabColor)
                .setOnClickListener(this);

        binding.tabMore.setTitle(getString(R.string.more))
                .setIcon(R.drawable.ic_more)
                .setTint(unselectedTabColor)
                .setOnClickListener(this);
    }

    private void initNavViewPager() {
        binding.viewPager.setOffscreenPageLimit(4);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(HomeFragment.newInstance(), "");
        viewPagerAdapter.addFragment(PlanFragment.newInstance(), "");
        viewPagerAdapter.addFragment(RunFragment.newInstance(), "");
//        viewPagerAdapter.addFragment(SettingFragment.newInstance(), "");
        viewPagerAdapter.addFragment(MoreFragment.newInstance(), "");
        binding.viewPager.addOnPageChangeListener(onPageChangeListener);
        binding.viewPager.setAdapter(viewPagerAdapter);

        onPageChangeListener.onPageSelected(0);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        BottomNavigationItemView itemView;

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    MyApplication.addJobInBackground(new GetProfileJob(hashCode()));
                    itemView = setSelectedTab(itemView, binding.tabHome);
                    break;
                case 1:
                    itemView = setSelectedTab(itemView, binding.tabPlan);
                    break;
                case 2:
                    itemView = setSelectedTab(itemView, binding.tabRun);
                    break;
                case 3:
                    itemView = setSelectedTab(itemView, binding.tabMore);
//                    itemView = setSelectedTab(itemView, binding.tabSetting);
                    break;
//                case 4:
//                    itemView = setSelectedTab(itemView, binding.tabMore);
//                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private BottomNavigationItemView setSelectedTab(BottomNavigationItemView previousTab, BottomNavigationItemView selectedTab) {
        if (previousTab == null) {
            selectedTab.setTint(selectedTabColor);
            return selectedTab;
        } else if (previousTab != selectedTab) {
            previousTab.setTint(unselectedTabColor);
            selectedTab.setTint(selectedTabColor);
            return selectedTab;
        }
        return previousTab;
    }


    @Override
    public void onClick(View view) {
        if (view == binding.tabHome) {
            binding.viewPager.setCurrentItem(0);
        } else if (view == binding.tabPlan) {
            binding.viewPager.setCurrentItem(1);
        } else if (view == binding.tabRun) {
            binding.viewPager.setCurrentItem(2);
//        } else if (view == binding.tabSetting) {
//            binding.viewPager.setCurrentItem(3);
        } else if (view == binding.tabMore) {
            binding.viewPager.setCurrentItem(3);
//            binding.viewPager.setCurrentItem(4);
        }
    }

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            ToastUtil.show(this, getString(R.string.tap_again_exit));
        }

        mBackPressed = System.currentTimeMillis();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionUtil.isPermissionHasGranted(grantResults)) {
            EventBusUtil.post(new PermissionEvent.OnPermissionGranted(requestCode));
        }
    }
}

