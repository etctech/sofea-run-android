package com.sofea.runapp.ui.plan;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewPlanItemBinding;
import com.sofea.runapp.model.Plan;

public class PlanItemView extends FrameLayout {

    private ViewPlanItemBinding binding;

    public PlanItemView(@NonNull Context context) {
        super(context);
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                    R.layout.view_plan_item,
                    this,
                    true);
        }
    }

    public void update(Plan plan) {
        binding.background.setImageResource(plan.getDrawableResId());
        binding.title.setText(getContext().getString(plan.getTitleResId()).toUpperCase().replace(" ", ""));
        binding.title.setTextColor(ContextCompat.getColor(getContext(), plan.getTitleColorResId()));
        binding.filter.setVisibility(plan.isEnabled() ? GONE : VISIBLE);
    }
}
