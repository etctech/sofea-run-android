package com.sofea.runapp.ui.event;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewEventHeaderBinding;
import com.sofea.runapp.util.ImageLoader;

public class EventHeaderView extends FrameLayout {

    private ViewEventHeaderBinding binding;

    public EventHeaderView(@NonNull Context context) {
        super(context);
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                    R.layout.view_event_header,
                    this,
                    true);
        }
    }

    public void update(String imageUrl, String title) {
        ImageLoader.glideImageLoadCenterCrop(binding.image, R.drawable.splash_screen_icon, imageUrl);
        binding.title.setText(title);
    }
}
