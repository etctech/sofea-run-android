package com.sofea.runapp.ui.notification;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ActivityListViewBinding;
import com.sofea.runapp.model.Notification;
import com.sofea.runapp.ui.BaseActivity;
import com.sofea.runapp.util.Util;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, NotificationActivity.class);
        context.startActivity(starter);
    }

    private ActivityListViewBinding binding;
    private NotificationAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_view);
        setTintBackButtonIcon(R.color.white, R.drawable.ic_back);
        setDisplayHomeAsUpEnabled();
        setBackNavigation();
        setToolbarTitle(R.string.notifications);

        adapter = new NotificationAdapter(this, new ArrayList<Notification>());
        binding.listView.setAdapter(adapter);
        binding.listView.hasMorePages(false);

        int pad8 = Util.dpToPx(this, 12);
        binding.listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
        binding.listView.setDividerHeight(pad8);
        binding.listView.setPadding(0, pad8, 0, pad8);
        binding.listView.setClipToPadding(false);

        List<Notification> notifications = new ArrayList<>();
        notifications.add(new Notification().setTitle("Oksana is joining your Runner")
                .setCreatedDate("2018-07-24 00:00:01")
                .setImageUrl("https://klyker.com/wp-content/uploads/2016/10/oksana-neveselaya-sexy-math-teacher-26.jpg"));
        notifications.add(new Notification().setTitle("Andy Johnson, Regis Weber and 3 other is joining your Runner")
                .setCreatedDate("2018-07-23 00:00:01")
                .setImageUrl("https://www.telegraph.co.uk/content/dam/fashion/2015-%2007/July17/lucky-blue-smith.jpg?imwidth=450"));
        notifications.add(new Notification().setTitle("Andy Johnson, Regis Weber and 3 other is joining your Runner")
                .setCreatedDate("2018-07-22 00:20:01")
                .setImageUrl("https://i.ytimg.com/vi/Umzgo0KP8UE/maxresdefault.jpg"));

        adapter.addAll(notifications);

        Util.setViewShown(binding.progressContainer, false);
        binding.swipeRefresh.setEnabled(false);
    }
}
