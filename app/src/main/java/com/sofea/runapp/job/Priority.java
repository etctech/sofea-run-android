package com.sofea.runapp.job;


public class Priority {

    public static final int HIGH = 3;
    public static final int MEDIUM = 2;
    public static final int LOW = 1;
}
