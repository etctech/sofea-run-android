package com.sofea.runapp.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

public abstract class GeneralJob extends BaseJob {
    protected int hashCode;

    protected GeneralJob(int hashCode, Params params) {
        super(params);
        this.hashCode = hashCode;
    }

    @Override
    public void onAdded() {

    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        handleThrowable(throwable, hashCode);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }

        return RetryConstraint.CANCEL;
    }
}
