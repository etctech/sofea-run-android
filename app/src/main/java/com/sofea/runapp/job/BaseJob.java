package com.sofea.runapp.job;


import android.content.Context;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.sofea.runapp.R;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.model.APIResult;
import com.sofea.runapp.util.EventBusUtil;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;


public abstract class BaseJob extends Job {
    protected BaseJob(Params params) {
        super(params);
    }

    protected String getDefaultFailedMessage() {
        return getApplicationContext().getString(R.string.something_went_wrong);
    }

    protected boolean shouldRetry(Throwable throwable) {
        return throwable instanceof UnknownHostException || throwable instanceof TimeoutException;
    }

    protected void handleThrowable(Throwable throwable, int hashCode) {
        Context context = getApplicationContext();

        if (throwable instanceof TimeoutException || throwable instanceof SocketTimeoutException) {
            EventBusUtil.post(new ExceptionEvent(context.getString(R.string.server_busy), hashCode));
        } else if (throwable instanceof ConnectException || throwable instanceof UnknownHostException) {
            EventBusUtil.post(new ExceptionEvent(context.getString(R.string.check_connection), hashCode));
        } else {
            EventBusUtil.post(new ExceptionEvent(throwable.getCause().getMessage(), hashCode));
        }
    }

    protected void handleFailedRequest(APIResult result, int hashCode) {
        if (result != null) {
            EventBusUtil.post(new ExceptionEvent(result.getMessage(), hashCode));
        }
        EventBusUtil.post(new ExceptionEvent(getDefaultFailedMessage(), hashCode));
    }
}
