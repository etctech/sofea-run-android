package com.sofea.runapp.job;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.event.AccountEvent;
import com.sofea.runapp.event.ExceptionEvent;
import com.sofea.runapp.model.APIResult;
import com.sofea.runapp.model.User;
import com.sofea.runapp.refrofit.ServiceGenerator;
import com.sofea.runapp.refrofit.TaskService;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.UserHandler;

import retrofit2.Call;
import retrofit2.Response;

public class GetProfileJob extends BaseJob {
    private int hashCode;

    public GetProfileJob(int hashCode) {
        super(new Params(Priority.HIGH));
        this.hashCode = hashCode;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Context context = getApplicationContext();

        TaskService taskService = ServiceGenerator.createService(TaskService.class);
        Call<APIResult> call = taskService.getProfile();
        Response<APIResult> execute = call.execute();

        APIResult result = execute.body();
        if (execute.isSuccessful() && result != null) {
            if (Constants.APIStatus.SUCCESS == result.getStatus()) {
                User user = result.getUser();
                UserHandler.setUser(context, user);
                EventBusUtil.post(new AccountEvent.OnGetProfile(user, hashCode));
                return;
            }
        }
        handleFailedRequest(result, hashCode);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        handleThrowable(throwable, hashCode);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }

        return RetryConstraint.CANCEL;
    }


    @Override
    protected int getRetryLimit() {
        return 2;
    }
}