package com.sofea.runapp.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.event.EventEvent;
import com.sofea.runapp.model.APIResult;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.refrofit.ServiceGenerator;
import com.sofea.runapp.refrofit.TaskService;
import com.sofea.runapp.util.EventBusUtil;

import retrofit2.Call;
import retrofit2.Response;

public class GetEventDetailsJob extends BaseJob {

    private int eventId;
    private int hashCode;

    public GetEventDetailsJob(int eventId, int hashCode) {
        super(new Params(Priority.HIGH));
        this.eventId = eventId;
        this.hashCode = hashCode;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        TaskService taskService = ServiceGenerator.createService(TaskService.class);
        Call<APIResult> call = taskService.getEventDetails(eventId);
        Response<APIResult> execute = call.execute();

        APIResult result = execute.body();
        if (execute.isSuccessful() && result != null) {
            if (Constants.APIStatus.SUCCESS == result.getStatus()) {
                Event event = result.getEvent();
                EventBusUtil.post(new EventEvent.OnGetEvent(event, hashCode));
                return;
            }
        }
        handleFailedRequest(result, hashCode);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        handleThrowable(throwable, hashCode);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }

        return RetryConstraint.CANCEL;
    }


    @Override
    protected int getRetryLimit() {
        return 2;
    }
}