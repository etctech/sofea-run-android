package com.sofea.runapp.job;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.event.EventsEvent;
import com.sofea.runapp.model.APIResult;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.refrofit.ServiceGenerator;
import com.sofea.runapp.refrofit.TaskService;
import com.sofea.runapp.util.EventBusUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class JoinEventJob extends GeneralJob {
    private int eventId;

    public JoinEventJob(int eventId, int hashCode) {
        super(hashCode, new Params(Priority.MEDIUM));
        this.eventId = eventId;
    }

    @Override
    public void onRun() throws Throwable {
        TaskService taskService = ServiceGenerator.createService(TaskService.class);
        Call<APIResult> call = taskService.getJoinEvent(eventId);
        Response<APIResult> execute = call.execute();

        APIResult result = execute.body();
        if (execute.isSuccessful() && result != null) {
            if (Constants.APIStatus.SUCCESS == result.getStatus()) {
                EventBusUtil.post(new EventsEvent.OnJoinEvent(hashCode));
                return;
            }
        }
        handleFailedRequest(result, hashCode);
    }
}
