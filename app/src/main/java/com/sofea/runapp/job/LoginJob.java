package com.sofea.runapp.job;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sofea.runapp.MyApplication;
import com.sofea.runapp.event.AccountEvent;
import com.sofea.runapp.model.User;
import com.sofea.runapp.util.EventBusUtil;
import com.sofea.runapp.util.UserHandler;


public class LoginJob extends BaseJob {
    private final User user;
    private final int hashCode;

    public LoginJob(User user, int hashCode) {
        super(new Params(Priority.HIGH));
        this.user = user;
        this.hashCode = hashCode;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        Context context = getApplicationContext();
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdmNDgwMzY5YThiNTIyMTRlZGU2YWZhODI2NTZkMDcxMjRiMmE4MzlmNmUyOThiZGZlMjE2OTU2MzZlYTA3YjYyNjEyZmYwZmI2ODk2YWQ1In0.eyJhdWQiOiI1IiwianRpIjoiN2Y0ODAzNjlhOGI1MjIxNGVkZTZhZmE4MjY1NmQwNzEyNGIyYTgzOWY2ZTI5OGJkZmUyMTY5NTYzNmVhMDdiNjI2MTJmZjBmYjY4OTZhZDUiLCJpYXQiOjE1MzI5MTkxMjQsIm5iZiI6MTUzMjkxOTEyNCwiZXhwIjoxNTY0NDU1MTI0LCJzdWIiOiIxNiIsInNjb3BlcyI6W119.Kc2hPoSNxtXB3SpT6d_su9XGXhPPBLo2LIaNCcx4g1KMVPROoCYAJuK7xUxIHSaaFiNis27nrbvbBjtqeElupmF5m7lKFInwLcm_vgU8OmNsYZ6nMysUKuMCx48_R5xe24Os7bSV95u63hEa7Z5WyrZItFqHJ1lpBoN8KngWlM_O7_hSS7txd0JiZzM1OrEDo4Uvtn9IrHK_WeijfBXlnMP742eWDzfLKowmOzzDvKUabX45nYPvKt93olLe42GR03_PkjXZZ5YbUqBYhuMi5om1SEQLK1da95KPWJuyc6T3Kj5fILXjMKE3-MfvIJmir8Od7_CU9OBN-JfSdIfK7-wXOPOwKKhvmt2lqp-W0dlvyHb7Yf9mkb39dKMaAfffp3DPAWwITyVtjBmaTyvg-UXQt1OJSw9Gh2FPxvAgsnVhR_xpLsIkxNwtEU9fhLra-zexbzyV6ac7bj1jp5EWM6KkvdQ6XO4GicFt-zvYSIi5ahmht-GEseDk_Npx-UMg3RNlLZZL7TmFdDVRjTpZm2WYrxKUEMmLm9G_BILtv95G_oJVZmlD5y1u4-jdAcNdYaFZpsqKH53v9GdKYDlwFtKzCw9hIc9Rha7YMeWzb0dy3VnIM4_9e2s-LMnbCL_3Z1p-F1262hHjlAPy2gEIBrXuvr436-VioiONQOtQtZU";
        UserHandler.setAccessToken(context, token);
        EventBusUtil.post(new AccountEvent.OnLogin(hashCode));

//
//        TaskService taskService = ServiceGenerator.createService(TaskService.class);
//        Call<APIResult> call = taskService.login(
//                user.getEmail(),
//                user.getLoginType(),
//                user.getDeviceId(),
//                user.getThirdPartyId(),
//                user.getDeviceOS());
//        Response<APIResult> execute = call.execute();
//
//        APIResult result = execute.body();
//        if (execute.isSuccessful() && result != null) {
//            if (Constants.APIStatus.SUCCESS == result.getStatus()) {
//                UserHandler.setIsRunning(context, result.getToken());
//                EventBusUtil.post(new AccountEvent.OnLogin(hashCode));
//            } else {
//
//            }
//        }
//        EventBusUtil.post(new ExceptionEvent(getDefaultFailedMessage(), hashCode));
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        handleThrowable(throwable, hashCode);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }

        return RetryConstraint.CANCEL;
    }


    @Override
    protected int getRetryLimit() {
        return 2;
    }
}