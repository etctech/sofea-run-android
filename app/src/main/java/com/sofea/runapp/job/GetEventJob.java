package com.sofea.runapp.job;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.event.EventsEvent;
import com.sofea.runapp.model.APIResult;
import com.sofea.runapp.model.Event;
import com.sofea.runapp.refrofit.ServiceGenerator;
import com.sofea.runapp.refrofit.TaskService;
import com.sofea.runapp.util.EventBusUtil;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class GetEventJob extends GeneralJob {

    public GetEventJob(int hashCode) {
        super(hashCode, new Params(Priority.HIGH));
    }

    @Override
    public void onRun() throws Throwable {
        TaskService taskService = ServiceGenerator.createService(TaskService.class);
        Call<APIResult> call = taskService.getEventList();
        Response<APIResult> execute = call.execute();

        APIResult result = execute.body();
        if (execute.isSuccessful() && result != null) {
            //if (Constants.APIStatus.SUCCESS == result.getStatus()) {
                ArrayList<Event> eventList = result.getEvents();
                EventBusUtil.post(new EventsEvent.OnGetAllEvents(eventList, hashCode));
                return;
            //}
        }
        handleFailedRequest(result, hashCode);
    }
}
