package com.sofea.runapp;

public class GitHubRepo {
    private int id;
    private String name;

    public GitHubRepo(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}