package com.sofea.runapp.util.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sofea.runapp.R;
import com.sofea.runapp.common.Constants;
import com.sofea.runapp.ui.MainActivity;
import com.sofea.runapp.util.run.RunHandler;
import com.sofea.runapp.util.run.TimeUtil;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class LocationMonitoringService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    private static final String TAG = LocationMonitoringService.class.getSimpleName();
    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest = new LocationRequest();
    private static Timer timer;

    public static final String DATA_ARGUMENT = "data_argument";
    public static final String START_RUNNING = "start_running";
    public static final String ACTION_LOCATION_BROADCAST = LocationMonitoringService.class.getName() + "LocationBroadcast";
    public static final String LOCATION = "location";
    public static final String TOTAL_DISTANCE = "total_distance";
    public static final String DURATION_SEC = "duration_sec";
    private static boolean hasStarted = false;
    private static int totalSec;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest.setInterval(Constants.LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(Constants.FASTEST_LOCATION_INTERVAL);


        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;

        mLocationRequest.setPriority(priority);
        mLocationClient.connect();

        registerReceiver();

        hasStarted = RunHandler.isRunning(getApplicationContext());
        totalSec = RunHandler.getDuration(getApplicationContext());
        startTimer();

        return START_STICKY;
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DATA_ARGUMENT);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DATA_ARGUMENT.equalsIgnoreCase(action)) {
                hasStarted = intent.getBooleanExtra(START_RUNNING, false);
                startTimer();
            }
        }
    };

    private void startTimer() {
        if (hasStarted) {
            if (timer == null) {
                timer = new Timer();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        totalSec += 1;
                        sendDurationToUI();
                        showNotification();
                    }
                }, 0, 1000);
            }
        } else {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            totalSec = 0;
        }
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;

    private void showNotification() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        String message = getApplicationContext().getString(R.string.duration) + " : " +
                TimeUtil.getDisplayRunTime(totalSec) +
                "\n" +
                getApplicationContext().getString(R.string.distance) + ": " +
                String.format(Locale.ENGLISH, "%.2f", RunHandler.getTotalKM(getApplicationContext())) + " km";

        if (notificationBuilder == null) {
            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    notificationIntent, 0);

            notificationBuilder = new NotificationCompat.Builder(this)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentTitle(getApplicationContext().getString(R.string.app_name))
                    .setTicker(getApplicationContext().getString(R.string.app_name))
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setPriority(Notification.PRIORITY_MAX);

        } else {
            notificationBuilder
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message);
        }
//                    startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notificationBuilder.build());
        notificationManager.notify(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notificationBuilder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        Log.d(TAG, "Connected to Google API");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            RunHandler.addRoute(getApplicationContext(), location);
            sendLocationToUI(location);
        }
    }

    private void sendLocationToUI(Location location) {
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(LOCATION, location);
        intent.putExtra(START_RUNNING, hasStarted);
        intent.putExtra(TOTAL_DISTANCE, RunHandler.getTotalKM(getApplicationContext()));
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        if (hasStarted) {
            RunHandler.setDuration(getApplicationContext(), totalSec);
            showNotification();
        }
    }

    private void sendDurationToUI() {
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(DURATION_SEC, totalSec);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Failed to connect to Google API");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}