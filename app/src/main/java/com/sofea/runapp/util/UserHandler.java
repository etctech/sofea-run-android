package com.sofea.runapp.util;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sofea.runapp.MyApplication;
import com.sofea.runapp.model.User;

import org.joda.time.DateTime;

import java.util.ArrayList;

public class UserHandler {

    private static final String ACCESS_TOKEN = "access_token";
    private static final String USER = "user";

    public static boolean isLogin(@NonNull Context context) {
        return !TextUtils.isEmpty(PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(ACCESS_TOKEN, ""));
    }

    public static boolean setAccessToken(@NonNull Context context, String accessToken) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(ACCESS_TOKEN, accessToken)
                .commit();
    }

    public static String getAccessToken(@NonNull Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(ACCESS_TOKEN, "");
    }

    public static boolean setUser(@NonNull Context context, User user) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(USER, user.toJson())
                .commit();
    }

    @Nullable
    public static User getUser(@NonNull Context context) {
        String userJson = PreferenceManager.getDefaultSharedPreferences(context).getString(USER, "");

        if (!TextUtils.isEmpty(userJson)) {
            return User.deserialize(userJson);
        }
        return null;
    }

    public static void clearAll(@NonNull Context context) {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .clear()
                .apply();
    }
}
