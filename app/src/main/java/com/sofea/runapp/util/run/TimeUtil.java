package com.sofea.runapp.util.run;

import java.util.Locale;

public class TimeUtil {

    public static String getDisplayRunTime(int totalSecs) {
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        return String.format(Locale.ENGLISH, "%02d:%02d", minutes, seconds);
    }
}
