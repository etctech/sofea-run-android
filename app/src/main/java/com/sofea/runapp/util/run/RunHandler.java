package com.sofea.runapp.util.run;

import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.sofea.runapp.model.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RunHandler {

    private static final String IS_RUNNING = "is_running";
    private static final String ROUTES = "routes";
    private static final String TOTAL_KM = "total_km";
    private static final String DURATION_IN_SEC = "duration_in_sec";

    public static boolean setIsRunning(@NonNull Context context, boolean isRunning) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(IS_RUNNING, isRunning)
                .commit();
    }

    public static boolean isRunning(@NonNull Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getBoolean(IS_RUNNING, false);
    }

    public static boolean addRoute(@NonNull Context context, Route route) {
        if (!isRunning(context)) {
            return false;
        }
        List<Route> routes = getRoutes(context);
        if (routes.size() > 0) {
            Route lastRoute = routes.get(routes.size() - 1);
            addTotalKM(context, DistanceUtil.distance(lastRoute.getLatitude(), lastRoute.getLongitude(),
                    route.getLatitude(), route.getLongitude()));
        }
        routes.add(route);
        return setRoutes(context, routes);
    }

    public static boolean setRoutes(@NonNull Context context, List<Route> routes) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putString(ROUTES, new Gson().toJson(routes))
                .commit();
    }

    public static boolean addRoute(@NonNull Context context, Location location) {
        return addRoute(context, new Route()
                .setLatitude(location.getLatitude())
                .setLongitude(location.getLongitude()));
    }

    public static Route getStartingRoute(@NonNull Context context) {
        List<Route> routes = getRoutes(context);
        if (routes.size() > 0) {
            return routes.get(0);
        }
        return null;
    }

    public static Route getLastRoute(@NonNull Context context) {
        List<Route> routes = getRoutes(context);
        if (routes.size() > 1) {
            return routes.get(routes.size() - 1);
        }
        return null;
    }

    @NonNull
    public static List<Route> getRoutes(@NonNull Context context) {
        String routeJson = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(ROUTES, "");
        if (!TextUtils.isEmpty(routeJson)) {
            return Route.deserializeList(routeJson);
        }
        return new ArrayList<>();
    }

    public static boolean addTotalKM(@NonNull Context context, double distance) {
        double totalKM = getTotalKM(context) + distance;
        return setTotalKm(context, totalKM);
    }

    public static boolean setTotalKm(@NonNull Context context, double distance) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putFloat(TOTAL_KM, (float) distance)
                .commit();
    }

    public static double getTotalKM(@NonNull Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getFloat(TOTAL_KM, 0);
    }

    public static boolean setDuration(@NonNull Context context, int durationInSec) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putInt(DURATION_IN_SEC, durationInSec)
                .commit();
    }

    public static int getDuration(@NonNull Context context) {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getInt(DURATION_IN_SEC, 0);
    }

    public static void stop(@NonNull Context context) {
        setDuration(context, 0);
        setTotalKm(context, 0);
        setRoutes(context, new ArrayList<Route>());
        setIsRunning(context, false);
    }

    public static String getAvgPace(@NonNull Context context) {
        int totalSec = getDuration(context);
        double totalKM = getTotalKM(context);
        if (totalKM < 0.01) {
            return "00:00";
        }
        double secPerKm = ((double) totalSec) / totalKM;
        return String.format(Locale.ENGLISH, "%02d", (int) (secPerKm / 60)) + ":" + String.format(Locale.ENGLISH, "%02d", (int) (secPerKm % 60));
    }
}
