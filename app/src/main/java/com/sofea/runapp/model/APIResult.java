package com.sofea.runapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class APIResult {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("event")
    @Expose
    private Event event;

    @SerializedName("events")
    @Expose
    private ArrayList<Event> events;

    public User getUser() {
        return user;
    }

    public Event getEvent() {
        return event;
    }

    public int getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }
}
