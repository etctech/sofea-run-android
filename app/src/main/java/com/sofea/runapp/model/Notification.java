package com.sofea.runapp.model;

public class Notification {

    private String imageUrl;
    private String title;
    private String createdDate;

    public String getImageUrl() {
        return imageUrl;
    }

    public Notification setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Notification setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public Notification setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
        return this;
    }
}
