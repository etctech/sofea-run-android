package com.sofea.runapp.model;

import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.sofea.runapp.R;

public class Plan {

    private int id;
    @DrawableRes
    private int drawableResId;
    @ColorRes
    private int titleColorResId;
    @StringRes
    private int titleResId;
    private boolean isEnabled;

    public Plan() {
    }

    public Plan(int id, int drawableResId, int titleColorResId, int titleResId) {
        this.id = id;
        this.drawableResId = drawableResId;
        this.titleColorResId = titleColorResId;
        this.titleResId = titleResId;
    }

    public int getId() {
        return id;
    }

    public Plan setId(int id) {
        this.id = id;
        return this;
    }

    public int getDrawableResId() {
        return drawableResId;
    }

    public Plan setDrawableResId(int drawableResId) {
        this.drawableResId = drawableResId;
        return this;
    }

    public int getTitleColorResId() {
        if (titleColorResId == 0)
            return R.color.black;
        return titleColorResId;
    }

    public Plan setTitleColorResId(int titleColorResId) {
        this.titleColorResId = titleColorResId;
        return this;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public Plan setTitleResId(int titleResId) {
        this.titleResId = titleResId;
        return this;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public Plan setEnabled(boolean enabled) {
        isEnabled = enabled;
        return this;
    }
}
