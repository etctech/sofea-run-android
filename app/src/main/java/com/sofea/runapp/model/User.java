package com.sofea.runapp.model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("device_os")
    @Expose
    private String deviceOs;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("third_party_type")
    @Expose
    private String thirdPartyType;
    @SerializedName("third_party_id")
    @Expose
    private String thirdPartyId;
    @SerializedName("total_min")
    @Expose
    private String totalMin;
    @SerializedName("total_sec")
    @Expose
    private String totalSec;
    @SerializedName("total_km")
    @Expose
    private String totalKM;
    @SerializedName("total_runs")
    @Expose
    private String totalRuns;
    @SerializedName("calories")
    @Expose
    private String calories;
    @SerializedName("invite_by")
    @Expose
    private String inviteBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    private String profileUrl;
    private double minPerSec;
    private String loginType;
    private String deviceOS;

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static User deserialize(String json) {
        return new Gson().fromJson(json, User.class);
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public User setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public User setLocation(String location) {
        this.location = location;
        return this;
    }

    public double getTotalKM() {
        if ("null".equalsIgnoreCase(totalKM) || TextUtils.isEmpty(totalKM)) {
            return 0.00;
        }
        return Double.parseDouble(totalKM);
    }

    public User setTotalKM(String totalKM) {
        this.totalKM = totalKM;
        return this;
    }

    public double getMinPerSec() {
        return minPerSec;
    }

    public User setMinPerSec(double minPerSec) {
        this.minPerSec = minPerSec;
        return this;
    }

    public int getTotalRuns() {
        if ("null".equalsIgnoreCase(totalRuns) || TextUtils.isEmpty(totalRuns)) {
            return 0;
        }
        return Integer.parseInt(totalRuns);
    }

    public User setTotalRuns(String totalRuns) {
        this.totalRuns = totalRuns;
        return this;
    }

    public int getCalories() {
        if ("null".equalsIgnoreCase(calories) || TextUtils.isEmpty(calories)) {
            return 0;
        }
        return Integer.parseInt(calories);
    }

    public User setCalories(String calories) {
        this.calories = calories;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public int getTotalMin() {
        if ("null".equalsIgnoreCase(totalMin) || TextUtils.isEmpty(totalMin)) {
            return 0;
        }
        return Integer.parseInt(totalMin);
    }

    public int getTotalSec() {
        if ("null".equalsIgnoreCase(totalSec) || TextUtils.isEmpty(totalSec)) {
            return 0;
        }
        return Integer.parseInt(totalSec);
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public String getLoginType() {
        return loginType;
    }

    public User setLoginType(String loginType) {
        this.loginType = loginType;
        return this;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public User setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
        return this;
    }
}
