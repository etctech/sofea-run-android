package com.sofea.runapp.model;

import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Route {

    private double longitude;
    private double latitude;

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static List<Route> deserializeList(String json) {
        return new Gson().fromJson(json, new TypeToken<ArrayList<Route>>() {
        }.getType());
    }

    public Route() {
    }

    public Route(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }

    public double getLongitude() {
        return longitude;
    }

    public Route setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Route setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }
}
