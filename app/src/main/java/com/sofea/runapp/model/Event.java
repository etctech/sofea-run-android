package com.sofea.runapp.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("img_path")
    @Expose
    private String coverUrl;

    @SerializedName("from")
    @Expose
    private String from;

    @SerializedName("to")
    @Expose
    private String to;

    @SerializedName("thumbnail_path")
    @Expose
    private String imageUrl;

    @SerializedName("name")
    @Expose
    private String title;

    @SerializedName("start_date")
    @Expose
    private String startDate;

    @SerializedName("start_time")
    @Expose
    private String startTime;

    @SerializedName("event_status")
    @Expose
    private String status;

    @SerializedName("joined")
    @Expose
    private int totalRunners;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("rewards")
    @Expose
    private String reward;

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static Event deserialize(String json) {
        return new Gson().fromJson(json, Event.class);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Event setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Event setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getStartDate() {
        return startDate;
    }

    public Event setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public Event setStatus(String status) {
        this.status = status;
        return this;
    }

    public int getTotalRunners() {
        return totalRunners;
    }

    public Event setTotalRunners(int totalRunners) {
        this.totalRunners = totalRunners;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public Event setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Event setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getReward() {
        return reward;
    }

    public Event setReward(String reward) {
        this.reward = reward;
        return this;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public Event setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
        return this;
    }

    public String getTo() {
        return to;
    }

    public String getFrom() {
        return from;
    }

    public int getId() {
        return id;
    }

    public String getStartTime() {
        return startTime;
    }
}
