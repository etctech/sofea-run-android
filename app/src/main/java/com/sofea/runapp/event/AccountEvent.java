package com.sofea.runapp.event;


import com.sofea.runapp.model.User;

public class AccountEvent {

    public static class OnLogin {
        private int hashCode;

        public OnLogin(int hashCode) {
            this.hashCode = hashCode;
        }

        public int getHashCode() {
            return hashCode;
        }
    }

    public static class OnGetProfile {
        private User user;
        private int hashCode;

        public OnGetProfile(User user, int hashCode) {
            this.user = user;
            this.hashCode = hashCode;
        }

        public User getUser() {
            return user;
        }

        public int getHashCode() {
            return hashCode;
        }
    }
}
