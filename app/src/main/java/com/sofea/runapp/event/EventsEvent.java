package com.sofea.runapp.event;


import com.sofea.runapp.model.Event;

import java.util.ArrayList;

public class EventsEvent {
    public static class OnGetAllEvents {
        private ArrayList<Event> list;
        private int hashCode;

        public OnGetAllEvents(ArrayList<Event> list, int hashCode) {
            this.list = list;
            this.hashCode = hashCode;
        }

        public ArrayList<Event> getList() {
            return list;
        }

        public int getHashCode() {
            return hashCode;
        }
    }

    public static class OnJoinEvent {
        private int hashCode;

        public OnJoinEvent(int hashCode) {
            this.hashCode = hashCode;
        }

        public int getHashCode() {
            return hashCode;
        }
    }
}
