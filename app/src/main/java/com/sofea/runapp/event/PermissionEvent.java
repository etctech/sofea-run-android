package com.sofea.runapp.event;

public class PermissionEvent {

    public static class OnPermissionGranted {
        private int requestCode;

        public OnPermissionGranted(int requestCode) {
            this.requestCode = requestCode;
        }

        public int getRequestCode() {
            return requestCode;
        }
    }
}
