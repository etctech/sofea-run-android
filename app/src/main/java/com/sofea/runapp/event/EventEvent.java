package com.sofea.runapp.event;

import com.sofea.runapp.model.Event;

public class EventEvent {
    public static class OnGetEvent {
        private Event event;
        private int hashCode;

        public OnGetEvent(Event event, int hashCode) {
            this.event = event;
            this.hashCode = hashCode;
        }

        public Event getEvent() {
            return event;
        }

        public int getHashCode() {
            return hashCode;
        }
    }
}
