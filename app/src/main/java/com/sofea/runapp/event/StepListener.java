package com.sofea.runapp.event;

// Will listen to step alerts
public interface StepListener {
    void step(long timeNs);

    void velocity(int strength);
}
