package com.sofea.runapp.refrofit;


import com.sofea.runapp.model.APIResult;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TaskService {

    @FormUrlEncoded
    @POST("/api/login")
    Call<APIResult> login(@Field("email") String email,
                          @Field("login_type") String loginType,
                          @Field("device_id") String deviceId,
                          @Field("third_party_id") String thirdPartyId,
                          @Field("device_os") String deviceOS);


    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    @GET("/api/user/profile")
    Call<APIResult> getProfile();

    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    @GET("/api/event")
    Call<APIResult> getEventList();

    @FormUrlEncoded
    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    @POST("/api/event/join")
    Call<APIResult> getJoinEvent(@Field("event_id") int eventId);

    @Headers({"Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"})
    @GET("/api/event/show/{event_id}")
    Call<APIResult> getEventDetails(@Path("event_id") int eventId);
}
