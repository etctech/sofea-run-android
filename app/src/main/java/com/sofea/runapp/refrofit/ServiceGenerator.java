package com.sofea.runapp.refrofit;


import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.sofea.runapp.BuildConfig;
import com.sofea.runapp.MyApplication;
import com.sofea.runapp.util.UserHandler;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGenerator {

    private static final String BASE_URL = BuildConfig.ENDPOINT;
    private static final String AUTHORIZATION = "Authorization";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addNetworkInterceptor(new StethoInterceptor())
                    .addNetworkInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Context context = MyApplication.getInstance().getApplicationContext();
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    if (UserHandler.isLogin(context)) {
                        requestBuilder.header(AUTHORIZATION, "Bearer " + UserHandler.getAccessToken(context));
                    }
                    return chain.proceed(requestBuilder.build());
                }
            }).build());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
