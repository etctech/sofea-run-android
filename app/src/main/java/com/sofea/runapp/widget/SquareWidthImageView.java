package com.sofea.runapp.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by etc03 on 08/11/2017.
 */

public class SquareWidthImageView extends AppCompatImageView {
    public SquareWidthImageView(Context context) {
        super(context);
    }

    public SquareWidthImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareWidthImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}
