package com.sofea.runapp.widget;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.sofea.runapp.R;
import com.sofea.runapp.databinding.ViewBottomNavigationItemBinding;
import com.sofea.runapp.util.Util;


/**
 * Created by etc03 on 13/12/2017.
 */

public class BottomNavigationItemView extends FrameLayout {
    public BottomNavigationItemView(@NonNull Context context) {
        super(context);
        init();
    }

    public BottomNavigationItemView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BottomNavigationItemView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private ViewBottomNavigationItemBinding binding;
    private int count;

    private void init() {
        if (!isInEditMode()) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()),
                    R.layout.view_bottom_navigation_item,
                    this,
                    true);
        }
    }

    public BottomNavigationItemView setTitle(String title) {
        binding.title.setText(title);
        return this;
    }

    public BottomNavigationItemView setIcon(@DrawableRes int drawableId) {
        binding.icon.setImageResource(drawableId);
        return this;
    }

    public BottomNavigationItemView setTint(@ColorRes int colorId) {
        int color = ContextCompat.getColor(getContext(), colorId);
        binding.title.setTextColor(color);
        binding.icon.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return this;
    }

    public BottomNavigationItemView setBadgeCount(int count) {
        this.count = count;
        if (count > 99) {
            count = 99;
        } else if (count == 0) {
            Util.setViewShown(binding.badge, false);
            return this;
        }
        Util.setViewShown(binding.badge, true);
        binding.badge.setText(String.valueOf(count));
        return this;
    }

    public BottomNavigationItemView minusCount(int minus) {
        if (count > 0) {
            count = count - minus;
        }
        binding.badge.setText(String.valueOf(count));
        return this;
    }
}
